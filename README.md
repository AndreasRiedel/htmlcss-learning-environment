# HTML und CSS Learning environment

Conception and realization of a prototypical learning environment for the automatic
evaluation of HTML- and CSS-based source code through front-end tests.

## Demo

![Recordit GIF](http://g.recordit.co/dtFGedgfoA.gif)

## Getting Started

```
$ npm install
$ npm start

```
Open [http://localhost:3000/](http://localhost:3000/) in your browser and naviagte to a task to solve it. 

Open [http://localhost:3000/admin](http://localhost:3000/admin) in your browser to create delete or update test scenarios.  

## More Info

On [http://localhost:3000/admin](http://localhost:3000/admin) you can find more information about the project. Please keep in mind that the description is in German.

## Usage

Test templates and an examples can be found/created in 
```
src/js/tests/
```
Explanation of the test files can be found in 
```
src/js/tests/testexample
```

## Built With

* [node.js](https://nodejs.org/en/)
* [MongoDB](https://www.mongodb.com/) 
* [Express JS](https://expressjs.com/)
* [Tape CSS](https://github.com/studio-b12/tape-css)

## Author

* **Andreas Riedel** - *Initial work* - [Andreas Riedel](https://bitbucket.org/AndreasRiedel)
