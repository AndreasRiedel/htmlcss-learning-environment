//self.apfel_foto7 – Apfel Foto,//self.apfel_positionieren9 – Apfel positionieren ,//self.apfel_einfaerben8 – Apfel einfärben,
const test1 = function () {
	let self = this;
	self.apfel_foto7 = (htmlNode, cssString, test, h, HelperInstance) => {
		let boolean = false;
		let info = h('span.info');
		let img = h('img', {src: '/img/apfel.jpg'});
		let rahmen = h('.rahmen', [img, info]);
		test(('check if image is there '), { dom: htmlNode }, (asset) => {
			let boolean = false;
			let imgUser = htmlNode.querySelector('img');
			imgUser !== null ? boolean = HelperInstance.htmlDifferences(img, imgUser , 'strict' ) : boolean = false;
			asset.ok(
				boolean, "Überprüfen Sie den Image Path"
			)
			asset.end();
		}); 
		return Promise.resolve();
	}

	self.apfel_einfaerben8 = (htmlNode, cssString, test, h, HelperInstance) => {
		self.apfel_foto7(htmlNode, cssString, test, h, HelperInstance)
		boolean = false;
		let info = h('span.info');
		let img = h('img', {src: '/img/apfel.jpg'});
		let rahmen = h('.rahmen', [img, info]);
		let boolean = false;
		test.only(('check if rahmen, info and image exits in right order'), {dom: htmlNode, styles: cssString}, (asset) => {
			let rahmenUser = htmlNode.querySelector('.rahmen');
			if(rahmenUser !== null) {
				HelperInstance.removeAllTextNodes(rahmenUser)
				boolean = HelperInstance.htmlDifferences(rahmen, rahmenUser);
			}
			asset.ok(boolean, "Überprüfen Sie das Markup ihrer Aufgabe");
			asset.end()
			
			if(boolean) {
				test(('check if rahmen has correct color'), {dom: htmlNode, styles: cssString}, (asset) => {
					let rahmenUser = htmlNode.querySelector('.rahmen');
					let backgroundColorRahmen = getComputedStyle(rahmenUser).getPropertyValue('background-color')
					asset.equal(
						backgroundColorRahmen, 'rgb(152, 251, 152)', "Bitte überprüfen Sie die Hintergrundfarbe des Rahmens"
					)
					asset.end();
				})

				test.onFailure(() => {
					console.log("failed")
				})
				
				test(('check if text has correct color'), {dom: htmlNode, styles: cssString}, (asset) => {
					let text = htmlNode.querySelector('.info');
					let backgroundColorText = getComputedStyle(text).getPropertyValue('background-color')
					asset.equal(
						backgroundColorText, 'rgb(139, 0, 0)', "Bitte überprüfen Sie die Hintergrundfarbe des Textes"
					)
					asset.end();
				})
			}
		})
		
		return Promise.resolve();
	}

	self.apfel_positionieren9 = (htmlNode, cssString, test, h, HelperInstance) => {
		self.apfel_foto7(htmlNode, cssString, test, h, HelperInstance)
		test(('check if image is relative'), {dom: htmlNode, styles: cssString}, (asset) => {
			
			let img = htmlNode.querySelector('img');
			let imgPos = HelperInstance.getStyleProperty('img', 'position')
			console.log(imgPos)
			asset.equal(
				 imgPos, 'relative', "Bitte überprüfen Sie ob das Bild die CSS-Regel relative besitzt"
			)
			asset.end();

		});

		test(('check if image top -20'), {dom: htmlNode, styles: cssString}, (asset) => {
			let img = htmlNode.querySelector('img');
			img.style.display = "block"
			console.log("top", HelperInstance.getPositionOfElementWindow(img).top)
			asset.equal(
				 HelperInstance.getPositionOfElementWindow(img).top, -20, "Bitte überprüfen Sie ob das Bild um 20px nach oben verschoben ist"
			)
			asset.end();

		});

		test(('check if image right 50'), {dom: htmlNode, styles: cssString}, (asset) => {
			let img = htmlNode.querySelector('img');
			img.style.display = "flex"
			
			console.log("right", img.offsetLeft)

			asset.equal(
				 img.offsetLeft, 50, "Bitte überprüfen Sie ob das Bild um 50px nach rechts verschoben ist"
			)
			asset.end();

		});
		return Promise.resolve();
	}
}

module.exports = test1;//self.apfel_foto7 – Apfel Foto,//self.apfel_positionieren9 – Apfel positionieren ,//self.apfel_einfaerben8 – Apfel einfärben,//self.apfel_foto7 – Apfel Foto,//self.apfel_positionieren9 – Apfel positionieren ,//self.apfel_einfaerben8 – Apfel einfärben,//self.apfel_foto7 – Apfel Foto,//self.apfel_positionieren9 – Apfel positionieren ,//self.apfel_einfaerben8 – Apfel einfärben,//self.apfel_foto7 – Apfel Foto,//self.apfel_positionieren9 – Apfel positionieren ,//self.apfel_einfaerben8 – Apfel einfärben,